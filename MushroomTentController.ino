/*
 *  @file
 *  Sketch for CO2, temperature and humidity sensor.
 *
 *  @author Javier Mansilla (javier@mansilla.info)
 *
 */

// Import Settings
#include "variables.h"                                                   // import variables

// Serial communication
#include <SoftwareSerial.h>

// JSON parser
#include <ArduinoJson.h>

// WIFI communication
#include <ESP8266WiFi.h>
//needed for library
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>         //https://github.com/tzapu/WiFiManager

// DHT
#include "DHT.h"
#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321
DHT dht(DHTPIN, DHTTYPE);

// Variables
int value = 0;
int ppm = 0;
float h = 0;
float t = 0;
int iterations = 1000; // High value to ensure the data delivery to the server
int CO2sensorMinutesON = 0;


SoftwareSerial mySerial(CO2RX, CO2TX);  // RX, TX respectively
WiFiManager wifiManager;


////////////////////////////////////////////////////
//    SETUP   //
////////////////////////////////////////////////////
void setup() {


  // Initialize the switch pins and set them ON

  pinMode(CORELAY, OUTPUT);
  pinMode(FANRELAY, OUTPUT);
  pinMode(HUMRELAY, OUTPUT);

  digitalWrite(CORELAY, HIGH);
  digitalWrite(FANRELAY, HIGH);
  digitalWrite(HUMRELAY, HIGH);

  
  // Initialize temp/hum sensor
  dht.begin();


  // Initialize serial pipes
  Serial.begin(115200);
   mySerial.begin(9600);

  /* Explicitly set the ESP8266 to be a WiFi-client, otherwise, it by default,
     would try to act as both a client and an access-point and could cause
     network-issues with your other WiFi-devices on your WiFi-network. */
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  // Set a timeout of 30 seconds (try 60 times) for connecting to the network
  int timeout = 0;
  while (WiFi.status() != WL_CONNECTED && timeout < 60) {
    delay(500);
    Serial.print(timeout);
    timeout++;

    // After trying 60 times, try with another network
    if (timeout == 60) {
      continue;
    }
  }
  // Config the wifi connection
  // We start by connecting to a WiFi network
  const char* networkName = "Karhu Helsinki - Sensor 5";
  wifiManager.autoConnect(networkName, "12345678");

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  delay(5000);

}





////////////////////////////////////////////////////
//    LOOP    //
////////////////////////////////////////////////////
void loop() {

  // Start a new iteration
  iterations++;
  CO2sensorMinutesON++;
  int start = millis();

  // Read the CO2 sensor and wairt
  readMHZ16();
  delay(500);

  readDHT();  
  int index = 0;
  // Read DHT up to 10 times if values are both 0, just to avoid reading errors
  while (h == 0 && t == 0 && index < 10) {
    readDHT();
    index++;
  }

  correctEnvironment();

  /*
    restablishingConnection();
    if (WiFi.status() != WL_CONNECTED) {
      ESP.deepSleep(5 * 1000000);
    }

    Serial.print("connecting to ");
    Serial.println(host);
  */

  if(iterations >= period) {
    sendDataToServer();
    iterations = 0;
  }


  // If CO2 sensor has been ON for 10 hours, reset it
  if(CO2sensorMinutesON >= 600) {
    digitalWrite(CORELAY, LOW);
    delay (15000);
    digitalWrite(CORELAY, HIGH);

    CO2sensorMinutesON = 0;
  }

  int duration = millis() - start;

  // Wait for 60 (or 120, if loop took longer than one minute) seconds minus execution time
  if(duration > 60000) {
    delay(120000 - duration);
  }
  else {
    delay(60000 - duration);
  }

}


////////////////////////////////////////////////////
//    Send Data to Server    //
////////////////////////////////////////////////////
void sendDataToServer() {
  // Use WiFiClient class to create TCP connections
  WiFiClient client;
  const int httpPort = 80;

  int connectionFails = 0;
  if (!client.connect(host, httpPort)) {
    Serial.println("connection failed");
    delay(10000);

    connectionFails++;

    if (connectionFails == 10) {
      ESP.deepSleep(5 * 1000000);
    }
    return;
  }



  // We now create a URI for the request
  value++;
  //String url = "/api/insertdata.php?hash=littlegarden&s=" + sensorID + "&t=" + String(t) + "&h=" + String(h) + "&co2=" + String(ppm) + "&p=" + String(pressure) + "&times=" + String(value);
  //String url = "/insertdata.php?hash=helsienifarm&s=" + sensorID + "&t=" + String(t) + "&h=" + String(h) + "&co2=" + String(ppm) + "&p=0&times=" + String(value);
  String url = path + "?hash=" + hash + "&s=" + sensorID + "&t=" + String(t) + "&h=" + String(h) + "&co2=" + String(ppm) + "&p=" + "&times=" + String(value);

  Serial.print("Requesting URL: ");
  Serial.println(url);



  // This will send the request to the server
  client.print(String("GET ") + url + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" +
               "Connection: close\r\n\r\n");


  unsigned long timeout = millis();
  while (client.available() == 0) {
    if (millis() - timeout > 10000) {
      Serial.println(">>> Client Timeout !");
      client.stop();
      return;
    }
  }

  // Read all the lines of the reply from server and print them to Serial
  String serverResponse = "";
  while (client.available()) {
    String line = client.readStringUntil('\r');
    serverResponse += line;
    //Serial.print(line);
  }

  int firstOpeningBracket = serverResponse.indexOf('{', 0);
  int lastClosingBracket = serverResponse.indexOf('}', 0);

  Serial.print(firstOpeningBracket);
  Serial.print(" - ");
  Serial.println(lastClosingBracket);

  String JSON = serverResponse.substring(firstOpeningBracket, lastClosingBracket + 1);
  Serial.print(JSON);
  DynamicJsonBuffer  jsonBuffer;
  JsonObject& root = jsonBuffer.parseObject(JSON);

  if (root.success()) {
    int p = (int)root["interval"];
    Serial.print("New period is: ");
    Serial.println(p);


    if (p > 59000) {
      period = p/60000;
      Serial.print("New period is: ");
      Serial.println(period);
    }

  }
  else {
    Serial.println("parseObject() failed");
  }


  Serial.println();
  Serial.println("closing connection");
  Serial.println();
  Serial.print("Sensor data sent: ");
  Serial.print(value);
  Serial.println(" times");

}


////////////////////////////////////////////////////
//    readDHT    //
////////////////////////////////////////////////////
void readDHT() {

  // Initialize old (globals) and new variables
  float t1, t2; // Temperature will be read twice
  float h1, h2; // Humidity will be read twice
  t = 0;        // Reset temperature
  h = 0;        // Reset humidity


  // Read temperature as Celsius (the default)
  h1 = dht.readHumidity();
  t1 = dht.readTemperature();

  // Wait, it's a slow sensor
  delay(1000);

  // If values are not a number, set both as 0
  if (isnan(h1) || isnan(t1)) {
    h1 = 0;
    t1 = 0;
  }

  // Read sesor again to check if values are consistent
  h2 = dht.readHumidity();
  t2 = dht.readTemperature();

  // Wait, it's a slow sensor
  delay(1000);

  // If values are not a number, set both as 0
  if (isnan(h2) || isnan(t2)) {
    h2 = 0;
    t2 = 0;
  }

  Serial.println(t1 - t2);
  Serial.println(h1 - h2);


  if (abs(t1 - t2) < 0.3 && abs(h1 - h2) < 1) {
    h = h1;
    t = t1;
  }

}

////////////////////////////////////////////////////
//    restablishingConnection    //
////////////////////////////////////////////////////
/*
  void restablishingConnection(){
      int timeout = 0;
      while (WiFi.status() != WL_CONNECTED && timeout < 60) {
          delay(500);
          Serial.print(timeout);
          timeout++;

          // After trying 60 times, try with another network
          if(timeout == 60) {
              if(ssid == ssid1) {
                  ssid     = ssid2;
                  password = password2;
              }
              else {
                  ssid     = ssid1;
                  password = password1;
              }

              WiFi.disconnect();
              delay(500);
              WiFi.begin(ssid, password);
              timeout = 0;
          }
      }

  }

*/



////////////////////////////////////////////////////
//    Correct environment    //
////////////////////////////////////////////////////
void correctEnvironment() {
  if (h <= minHum) {
    digitalWrite(HUMRELAY, HIGH);
    Serial.print("Switching the humidifier ON because humidity is ");
    Serial.print(h);
    Serial.println(" %");
  }
  else if (h >= maxHum) {
    digitalWrite(HUMRELAY, LOW);
    Serial.print("Switching the humidifier OFF because humidity is ");
    Serial.print(h);
    Serial.println(" %");
  }

  if (ppm <= minCO2) {
    digitalWrite(FANRELAY, LOW);
    Serial.print("Switching the fan OFF because CO2 level is ");
    Serial.print(ppm);
    Serial.println(" ppm");
  }
  else if (ppm >= maxCO2) {
    digitalWrite(FANRELAY, HIGH);
    Serial.print("Switching the fan ON because CO2 level is ");
    Serial.print(ppm);
    Serial.println(" ppm");
  } 
}


////////////////////////////////////////////////////
//    Read CO2 sensor    //
////////////////////////////////////////////////////
void readMHZ16() {

  char response[9];
  ppm = 0;
  int times = 0;


  do {
    // Restarting Serial communication with CO2 sensor to ensure it works fine
    SoftwareSerial mySerial(CO2RX, CO2TX);  // RX, TX respectively
    mySerial.begin(9600);
    mySerial.write(cmd, 9);
    delay(1000);
    mySerial.readBytes(response, 9);
    delay(1000);

    int responseHigh = (int) response[2];
    int responseLow = (int) response[3];
    ppm = (256 * responseHigh) + responseLow;

    Serial.print("CO2 concentration is: ");
    Serial.print(ppm);
    Serial.println(" ppm");
    times++;
    Serial.println(times);
  } while ((int)response[1] != 134 && times <= 1);

  // Sometimes, In case the value it's too high, the module needs to be resetted
  if (ppm > CO2range || ppm == 0 || times >= 10) {
    //ESP.deepSleep(5 * 1000000);
  }
}
