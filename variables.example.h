/**
 *   EDIT THIS FILE AND RENAME IT AS variables.h
 *
 */



// Set network configuration
const char* ssid      =   "NETWORK NAME";
const char* password  =   "NETWORK PASSWORD";

// If you want to send the data to the server, set variables below
const char* host      =   "www.mydomain.net";
String path           =   "/path/to/file.php";
String hash           =   "my-security-word-for-the-server";
String sensorID       =   "10";

// Variables to be modified
int period          = 5;          // Time between data logging
int CO2range        = 5000;
int maxCO2          = 800;
int minCO2          = 500;
int minHum          = 70;
int maxHum          = 90;


// Pins used
#define   DHTPIN        D6               // Pin where temp/hum sensor is connected
#define   CORELAY       D5               // Pin used to reset the CO2 sensor
#define   FANRELAY      D1               // Pin to control the fan
#define   HUMRELAY      D3               // Pin to control the humidifier
#define   CO2RX         D7               // Receiving pin for CO2 sensor
#define   CO2TX         D8               // Transmitting pin for CO2 sensor


//COMMANDS
//Get data from MH-Z16
byte cmd[9] = {0xFF, 0x01, 0x86, 0x00, 0x00, 0x00, 0x00, 0x00, 0x79};